
# Getting Started with Node.js on DHP

The following readme will be a walk-thru of getting started with putting your node.js application on to DHP. 

This guide will assume some familiarity with [node.js](https://nodejs.org/en/), [nvm](https://github.com/creationix/nvm), [npm](https://www.npmjs.com/), [express](https://expressjs.com/), etc. Please review the documentation on those tools if needed. Although this guide will use express.js, similar steps should work just as well for other node.js applications. 

As a reminder, you will need a DHP account. Please refer to the getting started section of the DHP website for more information on having an account or organization setup for you. 

## Step 1: Your node.js app

This guide assumes you have a node application. In this repo, I've just taken the express hello world app and added it to server.js. Your app will be way more complicated - and way more cool - but to get it on to DHP, the steps are roughly the same. 

It is best practice and often preferred to make sure your node application starts with npm start. It doesn't have to, but again, it helps other developers and services understand your app quicker. 

```bash
npm start
```

![npm start](./docs/npm-start.gif)


## Step 2: Deploy to DHP

DHP runs atop of [Pivital's Cloud Foundry](https://www.cloudfoundry.org/), as such, we utilize their tools and documentation and you can do the same. 

You'll want to make sure you have the latest PCF CLI. You can grab that [here](https://github.com/cloudfoundry/cli). 

### Login into Cloud Foundry

First, we need to authenticate with the cloud foundry api, so we can push our app to the right environment. In our case, we will be using the stage environment. 

```bash
cf login -a https://api.system.stage.dhp.bsci.com --skip-ssl-validation
Email> (your bsci email)
Password> (your bsci ldap password)
```

It will now prompt you to choose a space. A space for our purposes will be tied to a project or a CER, but your org may use spaces differently. 

If all goes well, you should see... 

```bash
API endpoint:   https://api.system.stage.dhp.bsci.com (API version: 2.98.0)
User:           <your email> 
Org:            <org>
Space:          <space>
```

### cf push

At this point, you should now simply be able to do the following. 

```bash
cf push my-sweet-app
```

Note that cloud foundry doesn't actually require you to put anything extra into your node.js project if your project is setup using industry best practices. In this case, if it sees a package.json in the root of the project, it assumes node.js and uses the latest stable buildpack. Pretty cool, eh?  

### Seeing your app in apps manager

You can login to apps manager using your browser - https://apps.system.stage.dhp.bsci.com/ - and see your application running. 

![apps man](./docs/appsman-app-started.gif)


## Starting and stopping your app

You may want to start and stop your app occasionally. This is useful when you don't want someone to see your app, you need to restart it, or you just don't want to use up resources.

When you do a cf push, it automatically starts your app, so let's go ahead and stop it. 

### cf stop

```bash
cf stop my-sweet-app
```

![cf stop](./docs/cf-stop.gif)

### cf start

```bash
cf start my-sweet-app
```

![cf start](./docs/cf-start.gif)


## Additional resources to help on your journey

* https://docs.cloudfoundry.org/buildpacks/node/node-tips.html
* https://expressjs.com/en/starter/hello-world.html


